package br.com.avaliacao.checkout.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CartItemTest {
		
	private CartItem cartItem;
	
	private ProdutoTest testaProduto;
	public static final Integer QUANTIDADE = 1;
	private Produto produto;

	@Before
	public void before() {
		testaProduto = new ProdutoTest();
		this.produto = testaProduto.before();
		cartItem = new CartItem();
		cartItem.setQuantity(QUANTIDADE);
		cartItem.setProduto(produto);		
	}

	@Test
	public void testAdicionaUmProduto() {
		Assert.assertEquals(produto, cartItem.getProduto());		
		Assert.assertEquals(QUANTIDADE, cartItem.getQuantity());		
	}
	
	@Test
	public void testAdicionaQuantidade() {
		cartItem.incrementQuantity(3);
		Assert.assertEquals(Integer.valueOf("4"), cartItem.getQuantity());		

	}
}
