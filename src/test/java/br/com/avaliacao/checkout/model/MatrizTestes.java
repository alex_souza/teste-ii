package br.com.avaliacao.checkout.model;

public class MatrizTestes {
	
	private Produto produto;
	private CartItem itemCarrinho;
	
	public Produto produtoUm() {
		this.produto = new Produto();
		this.produto.setCodigo("1");
		this.produto.setMarca("Nike");
		this.produto.setNome("Tenis Eastham");
		this.produto.setPreco(199.90);
		return produto;
	}
	
	public Produto produtoDois() {
		this.produto = new Produto();
		this.produto.setCodigo("2");
		this.produto.setMarca("Adidas");
		this.produto.setNome("Tenis ADI Ease Surf");
		this.produto.setPreco(139.90);
		return produto;
	}
	
	public CartItem itemUm() {
		this.itemCarrinho = new CartItem();
		this.itemCarrinho.setProduto(produtoUm());
		this.itemCarrinho.setQuantity(1);
		return itemCarrinho;
	}
	
	public CartItem itemDois() {
		this.itemCarrinho = new CartItem();
		this.itemCarrinho.setProduto(produtoDois());
		this.itemCarrinho.setQuantity(2);
		return itemCarrinho;
	}
}
