package br.com.avaliacao.checkout.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.avaliacao.checkout.model.Cart;
import br.com.avaliacao.checkout.model.Cart.CartStatus;
import br.com.avaliacao.checkout.model.CartItem;

public class CartTest {
	
	private Cart cart;
	private MatrizTestes mTestes;
	private List<CartItem> itens = new ArrayList<>();
	
	@Before
	public void before() {
		this.cart = new Cart();
		this.mTestes = new MatrizTestes();
		this.itens.add(mTestes.itemUm());
		this.itens.add(mTestes.itemDois());
	}
	
	@Test
	public void adicionaCarrinho() {
		Assert.assertEquals(null, cart.getCartId());
		Assert.assertEquals(itens, cart.getItems());
		Assert.assertEquals(CartStatus.OPENED, cart.getStatus());
	}
	
	@Test
	public void testGetPreco() {
		cart.getItems().clear();
		Assert.assertEquals(new Double(0.00), cart.getPrice());
		
		CartItem item1 = new CartItem();
		item1.setProduto(mTestes.produtoUm());
		item1.setQuantity(2);
		
		cart.getItems().add(item1);
		
		Assert.assertEquals(new Double(500.00), cart.getPrice());
	}
}
