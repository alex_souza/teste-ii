package br.com.avaliacao.checkout.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.avaliacao.checkout.model.Produto;

public class ProdutoTest {
	
	private static final String CODIGO = "1";
	private static final String MARCA = "OLIMPIKUS";
	private static final String NOME = "TENIS DAILY";
	private static final Double PRECO = 119.90;

	
	private Produto produto;
	
	@Before
	public Produto before() {
		produto = new Produto();
		produto.setCodigo(CODIGO);
		produto.setMarca(MARCA);
		produto.setNome(NOME);
		produto.setPreco(PRECO);
		return produto;
	}
	
	@Test
	public void testCriaProduto() {
		Assert.assertEquals(CODIGO, produto.getCodigo());
		Assert.assertEquals(NOME, produto.getNome());
		Assert.assertEquals(MARCA, produto.getMarca());
		Assert.assertEquals(PRECO, produto.getPreco());
	}	

}
